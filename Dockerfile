FROM ubuntu:latest

# Set noninteractive installation to avoid getting stuck at prompts
ARG DEBIAN_FRONTEND=noninteractive

# Install required packages
RUN apt-get update && apt-get install -y \
    curl \
    libssl-dev \
    libudev-dev \
    pkg-config \
    build-essential \
    jq

# In production, this will be a pvc
RUN mkdir /data

# Clean up the apt cache to reduce image size
RUN rm -rf /var/lib/apt/lists/*

# Download and install the Solana release
ARG SOLANA_VERSION=stable
RUN sh -c "$(curl -sSfL https://release.solana.com/$SOLANA_VERSION/install)"

# Add Solana binaries to PATH
ENV PATH="/root/.local/share/solana/install/active_release/bin:$PATH"

# Expose ports (8899 for JSON RPC, 8900 for websocket, 8001 for entrypoint)
EXPOSE 8899 8900 8001

# Command to run the test validator
ENTRYPOINT ["solana-test-validator"]
CMD ["--ledger", "/data"]
